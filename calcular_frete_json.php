<?php

function valor_total($valor_produtos, $peso_produtos_kg) {
    $FRETE_KG = 10;

    $valor_frete = $FRETE_KG * $peso_produtos_kg;

    return $valor_produtos + $valor_frete;
}

$valor = $_REQUEST['valor_produtos'];
$peso = $_REQUEST['peso'];

$total = valor_total($valor, $peso);

header('Content-type: application/json');
echo json_encode(['total' => $total]);
