<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>01</h1>
        <?php
        
        function meu_maximo($a, $b, $c) {
            // OPÇÃO 1
//            if ($a > $b && $a > $c) {
//                return $a;
//            }
//            
//            if ($b > $a && $b > $c) {
//                return $b;
//            }
//            
//            if ($c > $a && $c > $b) {
//                return $c;
//            }
            
            
              // OPÇÃO 2
//            $maximo = 0;
//            if ($a > $b && $a > $c) {
//                $maximo = $a;
//            } else if ($b > $a && $b > $c) {
//                $maximo = $b;
//            } else if ($c > $a && $c > $b) {
//                $maximo = $c;
//            }
//
//            return $maximo;
            
            
//            // OPÇÃO 3
//            if ($a > $b && $a > $c) {
//                return $a;
//            }
//            
//            if ($b > $a && $b > $c) {
//                return $b;
//            }
//            
//            return $c;

            
            // OPÇÃO 4
//            if ($a > $b && $a > $c) {
//                return $a;
//            } else if ($b > $a && $b > $c) {
//                return $b;
//            } else {
//                return $c;
//            }

            
            // OPÇÃO 5
            $valores = [$a, $b, $c];
            $maximo = 0;
            foreach ($valores as $v) {
                if ($v > $maximo) {
                    $maximo = $v;
                }
            }
            
            return $maximo;
            
        }
        
        function meu_maximo_array($valores) {
            $maximo = 0;
            foreach ($valores as $v) {
                if ($v > $maximo) {
                    $maximo = $v;
                }
            }
            
            return $maximo;
        }
        
        ?>
        <p>Máximo = <?= meu_maximo(20, 5, 8) ?> (esperado 20)</p>
        <p>Máximo = <?= meu_maximo_array([20, 5, 8]) ?> (esperado 20)</p>
        
        
        
        
        <h1>Desafio primeira aula</h1>
        <?php
        
$TODOS_USUARIOS_BANCO = [];
$TODOS_USUARIOS_BANCO[] = ['idade' => 10, 'nome' => 'Kiko', 'cidade' => 'Rio Preto'];
$TODOS_USUARIOS_BANCO[] = ['idade' => 12, 'nome' => 'Madruga', 'cidade' => 'Acapulco'];
$TODOS_USUARIOS_BANCO[] = ['idade' => 30, 'nome' => 'Florinda', 'cidade' => 'Bady'];
$TODOS_USUARIOS_BANCO[] = ['idade' => 45, 'nome' => 'Chaves', 'cidade' => 'Rio Preto'];

function listarPorIdade($idadeMinima, $idadeMaxima) {
    global $TODOS_USUARIOS_BANCO;
    
    $resultado = [];
    
    foreach ($TODOS_USUARIOS_BANCO as $pessoa) {
        if ($pessoa['idade'] >= $idadeMinima && $pessoa['idade'] <= $idadeMaxima) {
            $resultado[] = $pessoa;
        }
    }
    
    return $resultado;
}

// Resultado esperado
/*
$pessoas_pelo_menos_30_anos = [
    ['idade' => 30, 'nome' => 'Florinda', 'cidade' => 'Bady'],
    ['idade' => 45, 'nome' => 'Chaves', 'cidade' => 'Rio Preto']
]
*/

$pessoas_pelo_menos_30_anos = listarPorIdade(30, 100);
var_dump($pessoas_pelo_menos_30_anos);
        ?>
    </body>
</html>
