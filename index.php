<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $x2 = [10, 20];
        //var_dump($x2);
        
        $x2[1000] = 'mil';
        $x2['nome'] = 'kiko';
        $x2[] = 'novo elemento';
        
        /*var_dump($x2);*/
        
        $x = [0 => 10, 1 => 20, 'nome' => 'kiko', 1000 => 'mil', ];
        //var_dump($x);
        
        /*
        foreach ($x as $indice => $elemento) {
            var_dump('indice ' . $indice . ' é o elemento ' . $elemento);
        }
        */
        
        //var_dump(count($x));
        
        $a = 1000;
        
        function soma($a, $b) {
            echo 'dentro da soma';
            var_dump($a);
            return $a + $b;
        }
        
        function soma_array($array) {
            $total = 0;
            foreach ($array as $elem) {
                $total = $total + $elem;
            }
            return $total;
        }

/*        
        echo soma_array([10, 20, 5]);
        
        $valor1 = 10;
        $s = soma($valor1, 2.5);
        echo "a soma é $s!";
*/
        
        function adicionar_pessoa(&$pessoas, $nome, $ano_nasc, $cidade) {
            $pessoas[] = ['nome' => $nome, 'idade' => 2018 - $ano_nasc,
                'cidade' => $cidade];
        }

        $pessoas = [];
        adicionar_pessoa($pessoas, 'madruga', 1935, 'rio preto');
        adicionar_pessoa($pessoas, 'madruga 2', 1935, 'rio preto');
        var_dump($pessoas);
        
        ?>
    </body>
</html>
