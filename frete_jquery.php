<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <table>
                <tr>
                    <td>
                        <label>Valor produtos</label>
                    </td>
                    <td>
                        <input id="valorProdutos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Peso (kg)</label>
                    </td>
                    <td>
                        <input id="peso"/>
                    </td>
                </tr>
            </table>
            
            <p>
                <button type="button" onclick="executarCalculo()">Calcular</button>
            </p>
            
            <p>
                Valor total = R$ <span id="resultado">?</span>
            </p>
        </form>
        
        <script>
            
            function valor_total(valor_produtos, peso_produtos_kg) {
                const FRETE_KG = 10;
                
                var valor_frete = FRETE_KG * peso_produtos_kg;
                
                return valor_produtos + valor_frete;
            }
            
            function executarCalculo() {
                // OPÇÃO 1
//                var elementoValorProdutos = $('#valorProdutos');
//                var textoValorProdutos = elementoValorProdutos.val();
//                var valorProdutos = Number(textoValorProdutos);
//                
//                var elementoPeso = $('#peso');
//                var textoElementoPeso = elementoPeso.val();
//                var peso = Number(textoElementoPeso);
//                
//                var total = valor_total(valorProdutos, peso);
//                
//                var elementoResultado = $('#resultado');
//                elementoResultado.html(total);
                
                // OPÇÃO 2
//                var valorProdutos = Number($('#valorProdutos').val());
//                var peso = Number($('#peso').val());
//                var total = valor_total(valorProdutos, peso);
//                $('#resultado').html(total);
                
                
                // OPÇÃO 3
                $('#resultado').html(valor_total(Number($('#valorProdutos').val()), Number($('#peso').val())));
            }
            
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    </body>
</html>
