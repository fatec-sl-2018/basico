<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <table>
                <tr>
                    <td>
                        <label>Valor produtos</label>
                    </td>
                    <td>
                        <input id="valorProdutos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Peso (kg)</label>
                    </td>
                    <td>
                        <input id="peso"/>
                    </td>
                </tr>
            </table>
            
            <p>
                <button type="button" onclick="executarCalculo()">Calcular</button>
            </p>
            
            <p>
                Valor total = R$ <span id="resultado">?</span>
            </p>
        </form>
        
        <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>

        <script>
            
            var minha_funcao_soma = function (a, b) {
                return a + b;
            };
            
            function executarCalculo() {
                var valorProdutos = Number($('#valorProdutos').val());
                var peso = Number($('#peso').val());
                
                
                $.ajax({
                    url: 'calcular_frete_json.php',
                    method: 'POST',
                    data: { valor_produtos: valorProdutos, peso: peso },
                    success: function (resposta) {
                        $('#resultado').html('<b>' + resposta.total + '</b>');
                    }
                });
            }
            
        </script>
    </body>
</html>
