<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <table>
                <tr>
                    <td>
                        <label>Valor produtos</label>
                    </td>
                    <td>
                        <input id="valorProdutos"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Peso (kg)</label>
                    </td>
                    <td>
                        <input id="peso"/>
                    </td>
                </tr>
            </table>
            
            <p>
                <button type="button" onclick="executarCalculo()">Calcular</button>
            </p>
            
            <p>
                Valor total = R$ <span id="resultado">?</span>
            </p>
        </form>
        
        <script>
            
            function valor_total(valor_produtos, peso_produtos_kg) {
                const FRETE_KG = 10;
                
                var valor_frete = FRETE_KG * peso_produtos_kg;
                
                return valor_produtos + valor_frete;
            }
            
            function executarCalculo() {
                var elementoValorProdutos = document.getElementById('valorProdutos');
                var textoValorProdutos = elementoValorProdutos.value;
                var valorProdutos = Number(textoValorProdutos);
                
                var elementoPeso = document.getElementById('peso');
                var textoElementoPeso = elementoPeso.value;
                var peso = Number(textoElementoPeso);
                
                var total = valor_total(valorProdutos, peso);
                
                var elementoResultado = document.getElementById('resultado');
                elementoResultado.innerHTML = total;
            }
            
        </script>
    </body>
</html>
