<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <script>
            
            function soma(a, b) {
                return a + b;
            }
            
            console.log('3 + 10 é', soma(3, 10));
            
            function soma_array(array) {
                var total = 0;
                var elem;
                for (var i = 0; i < array.length; i++) {
                    elem = array[i];
                    total = total + elem;
                }
                return total;
            }
            
            console.log(soma_array([10, 20, 30]));
            
            function adicionar_pessoa(pessoas, nome, ano_nasc, cidade) {
                pessoas.push({
                    nome: nome,
                    idade: 2018 - ano_nasc,
                    cidade: cidade
                });
            }
            
            minhas_pessoas = [];
            adicionar_pessoa(minhas_pessoas, 'madruga', 1935, 'rio preto');
            adicionar_pessoa(minhas_pessoas, 'kiko', 2001, 'catanduva');
            adicionar_pessoa(minhas_pessoas, 'chaves', 2000, 'bady');
            console.log(minhas_pessoas);
            
            function maximo(a, b, c) {
                if (a > b && a > c) {
                    return a;
                }

                if (b > a && b > c) {
                    return b;
                }

                return c;
            }
            
            console.log(maximo(20, 5, 1));
            console.log(maximo(1, 2, 3));
            
            function maximo_array(array) {
                var maximo;
                var i;
                var v;
                
                maximo = 0;
                for (i = 0; i < array.length; i++) {
                    v = array[i];
                    if (v > maximo) {
                        maximo = v;
                    }
                }

                return maximo;
            }

            console.log(
                    maximo_array(
                        [20, 5, 1]
                    )
            );
            console.log(maximo_array([1, 2, 1000, 3, 0]));

            function listar_por_idade(idadeMinima, idadeMaxima) {
                var resultado = [];
                var i;

                for (i = 0; i < minhas_pessoas.length; i++) {
                    var pessoa = minhas_pessoas[i];
                    if (pessoa.idade >= idadeMinima && pessoa.idade <= idadeMaxima) {
                        resultado.push(pessoa);
                    }
                }

                return resultado;
            }
            
            console.log(listar_por_idade(0, 30));
            
            function exemplos_var_let_const() {
                var a = 123;
                let b = 1000;
                const c = 'teste';
                
                console.log(a, b, c);
                
                if (true) {
                    // var a          -> apenas repetiu a declaração de cima
                    //       = 'eita  -> altera o valor da variável 'a'
                    var a = 'eita';
                    
                    // "esconde" a variável 'b' original, que continua existindo
                    let b = -99;
                    
                    //c = 'novo valor'; // proibido! dá erro
                    
                    console.log(a, b, c);
                    
                    // aqui a variável 'b' DESTE BLOCO deixa de existir
                }
                
                console.log(a, b, c);
            }
            exemplos_var_let_const();
            
            function valor_total(valor_produtos, peso_produtos_kg) {
                const FRETE_KG = 10;
                
                var valor_frete = FRETE_KG * peso_produtos_kg;
                
                return valor_produtos + valor_frete;
            }
            
            console.log(
                    '150 reais de produto + frete para produtos pesando 2kg = '
                    +
                    valor_total(150, 2)
            );
            
        </script>
    </body>
</html>
